using System;
using Itinero;
using Itinero.Osm.Vehicles;
using itineroBenchmark;


namespace stage
{
    class Program
    {
        static void Main(string[] args)
        {
            GeocentricCoordinatesLimit limiteCoord = new GeocentricCoordinatesLimit();
            limiteCoord.topLat =  51.16745977895161F;
            limiteCoord.leftLong = 3.7519423987661824F;
            limiteCoord.bottomLat = 50.601713935247204F;
            limiteCoord.rightLong = 4.960125403867238F;

            int numberOfRoutablePoints = 3000;

            ItineroBenchmark ib = new ItineroBenchmark(limiteCoord, numberOfRoutablePoints, Vehicle.Car);
            long timeEvaluation = ib.RoutingPerformance(Vehicle.Car.Fastest());
            Console.WriteLine(string.Format("{0:d} ms | {1:d} s", timeEvaluation, timeEvaluation / 1000));
            LimitedCoord(ib.routablePoints);
            Console.WriteLine(string.Format("{0:d} ms | {1:d} s", ib.timeToFindRandomRoutablePoint, ib.timeToFindRandomRoutablePoint / 1000));

            //Console.WriteLine(JObject.Parse(route.ToJson()));
            Console.WriteLine("----------------------------------------------------");
            //Console.WriteLine(JObject.Parse(route.ToGeoJson()));
        }

        static void LimitedCoord(RouterPoint[] rpArray)
        {
            foreach (RouterPoint rp in rpArray)
            {
                Console.WriteLine(string.Format("routable points : {0:d}", rp));

                if(rp.Latitude > 51.16745977895161F)
                    Console.WriteLine(string.Format("prob: {0:f}", rp.Latitude));
                if(rp.Longitude > 4.960125403867238F)
                    Console.WriteLine(string.Format("prob: {0:f}", rp.Longitude));
            }
        }
    }   
}
