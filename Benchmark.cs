using System;
using System.IO;
using System.Diagnostics;
using Itinero.IO.Osm;
using Itinero;
using Itinero.Profiles;

namespace itineroBenchmark
{
    public class ItineroBenchmark
    {
        private RouterDb routerDb;
        private Vehicle vehicle;
        private Random myRand = new Random();
        private Stopwatch stopwatch;
        private int numberOfFailingRoutage;
        private Router router;

        public GeocentricCoordinatesLimit coord {get; set;}
        public RouterPoint[] routablePoints {get; private set;}
        public int numberOfRoutablePoints {get; set;}
        public long timeToFindRandomRoutablePoint {get; private set;}


        public ItineroBenchmark(
            GeocentricCoordinatesLimit geocentricCoordinates,           
            int _numberOfRoutablePoints,
            Vehicle _vehicle
            )
        {
            LoadDataFromOSMfile();
            numberOfFailingRoutage = 0;
            stopwatch = new Stopwatch();
            coord = geocentricCoordinates;
            router = new Router(routerDb);
            vehicle = _vehicle;
            numberOfRoutablePoints = _numberOfRoutablePoints;
            routablePoints = new RouterPoint[numberOfRoutablePoints+1];
        }

        public long RoutingPerformance(Profile profile, int maxSnappingDistance = 100, bool withContraction = false){
            FindRandomRoutablePoint(profile, maxSnappingDistance);
            stopwatch.Reset();
            stopwatch.Start();
            int i = 0;
            int j = 0;
            foreach (RouterPoint rp in routablePoints)
            {
                router.Calculate(
                    profile, 
                    routablePoints[i].Latitude, 
                    routablePoints[i].Longitude, 
                    routablePoints[i+1].Latitude, 
                    routablePoints[i+1].Longitude
                );
                j++;
            }
            stopwatch.Stop();
            return stopwatch.ElapsedMilliseconds;
        }
        
        public void FindRandomRoutablePoint(Profile profile, int maxSnappingDistance = 100)
        {
            float latitudeDelta, longitudeDelta;
            Router router = new Router(routerDb);
            routablePoints[numberOfRoutablePoints] = router.Resolve(
                        profile,
                        50.85009296854335F,
                        4.35508303596801F,
                        maxSnappingDistance
                        );
            stopwatch.Reset();
            stopwatch.Start();

            for (int i = 0; i < numberOfRoutablePoints; i++)
            {
                latitudeDelta = GetRandomFloat(coord.bottomLat, coord.topLat);
                longitudeDelta = GetRandomFloat(coord.leftLong, coord.rightLong);

                try
                {
                    routablePoints[i] = router.Resolve(
                        profile,
                        coord.bottomLat + latitudeDelta,
                        coord.leftLong + longitudeDelta,
                        maxSnappingDistance
                        );
                }
                catch 
                {
                    numberOfFailingRoutage++;
                    i--;
                }               
            }
            stopwatch.Stop();
            Console.WriteLine("fin points trouvé");
            timeToFindRandomRoutablePoint = stopwatch.ElapsedMilliseconds;
        }

        private void LoadDataFromOSMfile()
        {
            if (!File.Exists(@"/home/rfr/Desktop/stage/belgium-latest.routerdb"))
            {
                routerDb = new RouterDb();
                using (var stream = new FileInfo(@"/home/rfr/Desktop/stage/belgium-latest.osm.pbf").OpenRead())
                {
                    routerDb.LoadOsmData(stream, this.vehicle);
                }
                using (var stream = new FileInfo(@"/home/rfr/Desktop/stage/belgium-latest.routerdb").Open(FileMode.Create))
                {
                    routerDb.Serialize(stream);
                }
            }
            else
            {
                using (var stream = new FileInfo(@"/home/rfr/Desktop/stage/belgium-latest.routerdb").OpenRead())
                {
                    routerDb = RouterDb.Deserialize(stream);
                }
            }
        }

        private float GetRandomFloat(float lowCoordinate, float highCoordinate)
        {
            return (float)myRand.NextDouble() * (highCoordinate - lowCoordinate);
        }
    }

    public struct GeocentricCoordinatesLimit
    {
        public float topLat;
        public float leftLong;
        public float bottomLat;
        public float rightLong;
    }
}
